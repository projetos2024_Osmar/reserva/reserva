const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesService")

cron.schedule("*/30 * * * * *", async()=>{
    try {
        await cleanUpSchedules();
        console.log("Limpeza automática executada");
    } catch (error) {
        console.error("Erro ao executar limpeza automática");        
    }
})